The Hyphenate module provides an input filter that integrates
the JavaScript library Hyphenator (http://code.google.com/p/hyphenator/).

The input text is wrapped in html tags with a configurable css class,
which defines the scope for the hyphenator. 

--------REQUIREMENTS---------
1. Libraries (http://drupal.org/project/libraries/)
2. Hyphenator: a Javascript library that implements client-side 
hyphenation of HTML-Documents (cf. INSTALLATION) 


---------INSTALLATION---------
1. Copy the hyphenator library (http://code.google.com/p/hyphenator/) to
your libraries directory. The path to the library should look like: 
/sites/all/libraries/hyphenator/Hyphenator.js
2. Copy hyphenate directory to your modules directory.
3. Enable the module at: /admin/build/modules.
4. Enable the hyphenate filter in the input format you want text to be hyphened.
5. Configure the settings in the input format configure tab. By Now the html tagname
for the wrapper tag and the css class.
